import Image from "next/image";
import Link from "next/link";
import React from "react";

export default function Home() {
  return (
    <>
      <div className="flex flex-col justify-center items-center h-screen">
        <h1 className="text-3xl text-center text-white">Welcome to YouApp</h1>
        <div className="flex flex-col justify-center items-center p-10">
          <Link href="/signin" className="btn btn-ghost text-white text-sm">Sign In</Link>
        </div>
      </div>
      
    </>
  )
}
