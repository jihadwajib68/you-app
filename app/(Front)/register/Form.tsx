'use client'
import { useRouter, useSearchParams } from 'next/navigation'
import { useEffect, useState } from 'react'
import { FiEye, FiEyeOff } from 'react-icons/fi'
import Link from 'next/link'
import Back from '@/components/Back'

type Inputs = {
  email: string
  username: string
  password: string
  confirmPassword: string
}

const Form = () => {
    const [ showPassword, setShowPassword ] = useState(false);
    const [ showConfirmPassword, setShowConfirmPassword ] = useState(false);


    const tooglePasswordVisibility = () => {
        setShowPassword((prevShowPassword) => !prevShowPassword);
    }

    const toogleConfirmPasswordVisibility = () => {
        setShowConfirmPassword((prevShowConfirmPassword) => !prevShowConfirmPassword);
    }

  return (
    <div className="bg max-w-lg mx-auto card flex flex-col">
        <Back />
        <div className="card-body justify-center">
        <h1 className="card-title text-white justify-between">Register</h1>
        <form>
          <div className="my-2">
            <input
              type="text"
              id="email"
              className="input input-bordered w-full max-w-sm text-white"
              placeholder='Enter Email'
            />
          </div>
          <div className="my-2">
            <input
              type="text"
              id="username"
              className="input input-bordered w-full max-w-sm text-white"
              placeholder='Enter Username'
            />
          </div>
          <div className="my-2">
            <input
              type={showPassword ? 'text' : 'password'}
              id="password"
              placeholder='Create Password'
              className="input input-bordered w-full max-w-sm text-white"
            /><span onClick={tooglePasswordVisibility} className='icon'>{showPassword ? <FiEyeOff /> : <FiEye />}</span>
            
          </div>
          <div className="my-2">
            <input
              type={showConfirmPassword ? 'text' : 'password'}
              id="confirmPassword"
              placeholder='Confirm Password'
              className="input input-bordered w-full max-w-sm text-white"
            /><span onClick={toogleConfirmPasswordVisibility} className='icon-confirm'>{showConfirmPassword ? <FiEyeOff /> : <FiEye />}</span>
            
          </div>
          <div className="my-4">
            <button
              type="submit"
              className="btn w-full text-white"
            >
              Login
            </button>
          </div>
        </form>
        <div className='acc flex items-center justify-center'>
          Have an account?{' '}
          <span className='span-link'>&nbsp;<Link className="link" href={`/signin`}>
            Login here
          </Link></span>
        </div>
      </div>
    </div>
  )
}
export default Form

